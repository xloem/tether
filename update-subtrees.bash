#!/usr/bin/env bash

# one tun library is at https://github.com/ambrop72/badvpn

{ cat <<SUBTREES
https://android.googlesource.com/platform/packages/modules/adb.git master android-adb
https://android.googlesource.com/platform/external/boringssl.git master android-boringssl
https://android.googlesource.com/platform/system/libbase.git master android-libbase
https://android.googlesource.com/platform/system/logging.git master android-logging
https://android.googlesource.com/platform/system/core.git master android-core
https://github.com/gsl-lite/gsl-lite.git master gsl-lite
SUBTREES
} | while read url branch subtree
do
    if [ -e "$subtree" ] 
    then
        git subtree pull --prefix="$subtree" "$url" "$branch"
    else
        git subtree add --prefix="$subtree" "$url" "$branch"
    fi
done
