#include <string>

#define PLATFORM_TOOLS_VERSION 0

namespace android { namespace build {

std::string GetBuildNumber()
{
    return std::to_string(PLATFORM_TOOLS_VERSION);
}

}}
